import blast
import json

def blastp_test_P01349():
    record = blast.blast(["blastp", "-query", f"/blast/queries/P01349.fasta", "-db", "pdbaa", "-outfmt", "15"])
    record = json.loads(record)
    hits = record["BlastOutput2"][0]["report"]["results"]["search"]["hits"]
    assert hits[0]["len"] == 27
    assert hits[0]["num"] == 1
    print(json.dumps(hits[0], indent=4))

blastp_test_P01349()