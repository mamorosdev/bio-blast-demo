efetch -db protein -format fasta \
    -id Q90523,P80049,P83981,P83982,P83983,P83977,P83984,P83985,P27950 \
    > fasta/nurse-shark-proteins.fasta

makeblastdb -in fasta/nurse-shark-proteins.fasta -dbtype prot \
    -parse_seqids -out blastdb_custom/nurse-shark-proteins -title "Nurse shark proteins" \
    -taxid 7801 -blastdb_version 5

efetch -db protein -format fasta -id P01349 > queries/P01349.fasta

blastp -query queries/P01349.fasta -db nurse-shark-proteins  -out results/blastp.out 

update_blastdb.pl -source gcp pdbaa

blastp -query queries/P01349.fasta -db pdbaa