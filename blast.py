import json
import os
from python_on_whales import docker

CWD = os.getcwd()

def blast(command):

    output = docker.run(
        image="ncbi/blast",
        remove=True,
        volumes=[(f"{CWD}/blast/blastdb", "/blast/blastdb"), (f"{CWD}/blast/blastdb_custom", "/blast/blastdb_custom"),
                 (f"{CWD}/blast/fasta", "/blast/fasta"), (f"{CWD}/blast/queries", "/blast/queries")],
        command=command)

    return output