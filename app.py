from flask import Flask, jsonify
import blast
import json

app = Flask(__name__, static_folder="out", static_url_path="/")

@app.route("/")
def index():
    return app.send_static_file("index.html")

@app.route("/api/time", methods=['GET'])
def get_time():
    return {"time": "Hello World"}

@app.route("/api/blastp/<prot_file>", methods=['GET'])
def blastp(prot_file):
    if not(prot_file):
        prot_file = "P01349.fasta"

    # if not (out_len):
    #     out_len = 15
    print(prot_file)
    record = blast.blast(["blastp", "-query", f"/blast/queries/P01349.fasta", "-db", "pdbaa", "-outfmt", "15"])
    record = json.loads(record)
    hits = record["BlastOutput2"][0]["report"]["results"]["search"]["hits"]
    return jsonify(hits[0])


if __name__ == "__main__":
    app.run()
